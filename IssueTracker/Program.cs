﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace IssueTracker
{
    class Program
    {
        private static void Write (string text)
        {
            var logPath = Path.GetFullPath(@"C:\Test\log.txt");
            using (var sw = File.AppendText(logPath))
            {
                sw.WriteLine(text);
            }
        }
        private static double ReadUserNumberDouble()
        {
            double number = 0;
            bool isSuccess = false;
            while (!isSuccess)
            {
                Console.WriteLine("Please enter a fist number: ");
                var userInput = Console.ReadLine();
                isSuccess = double.TryParse(userInput, out number);
                if (!isSuccess)
                {
                    Console.WriteLine("Error. You must input a number.");
                }
            }
            return number;
        }
        private static int ReadUserNumber()
        {
            int number = 0;
            bool isSuccess = false;
            while (!isSuccess)
            {
                Console.WriteLine("Please enter a second number: ");
                var userInput = Console.ReadLine();
                isSuccess = int.TryParse(userInput, out number);
                if (!isSuccess)
                {
                    Console.WriteLine("Error. You must input a number.");
                }
            }
            return number;
        }
        static void Main(string[] args)
        {
            double FirstNum = ReadUserNumberDouble();
            int SecNum = ReadUserNumber();
            double Res = Math.Pow(FirstNum, SecNum);           
            Console.WriteLine(FirstNum + "^" + SecNum + "=" + Res);            
            string line = Res.ToString();
            Write(line);           
            Console.ReadKey();
        }
    }    
}
